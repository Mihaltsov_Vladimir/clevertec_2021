package com.example.task_1

import android.app.Application
import android.util.Log

class App : Application() {

    lateinit var myViewModel: MyViewModel

    override fun onCreate() {
        super.onCreate()

        myViewModel = MyViewModel(Model(CashDataSource(this)))
    }
}