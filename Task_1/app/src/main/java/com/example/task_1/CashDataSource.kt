package com.example.task_1

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.MODE_PRIVATE

class CashDataSource(context: Context) : DataSource {

    private val sharedPreferences = context.getSharedPreferences("MyPref", MODE_PRIVATE)

    @SuppressLint("CommitPrefEdits")
    override fun saveInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }


    override fun getInt(key: String)=sharedPreferences.getInt(key,0)

}