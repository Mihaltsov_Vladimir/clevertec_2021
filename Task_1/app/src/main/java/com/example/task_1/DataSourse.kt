package com.example.task_1

interface DataSource {
    fun saveInt(key: String, value: Int)
    fun getInt(key: String): Int
}