package com.example.task_1

import java.util.*

class Model(private val dataSource: DataSource) {


    companion object {
        private const val COUNTER_KEY = "KEY"
    }

    private var timer: Timer? = null
    private val timerTask
        get() = object : TimerTask() {
            override fun run() {
                counter++
                collback?.updateString(counter.toString())
            }
        }

    private var collback: TextCollback? = null
    private var counter = -1


    fun start(textCollback: TextCollback) {
        collback = textCollback
        if (counter < 0)
            counter = dataSource.getInt(COUNTER_KEY)
        timer = Timer()
        timer?.scheduleAtFixedRate(timerTask, 0, 1000)
    }

    fun stop() {
        dataSource.saveInt(COUNTER_KEY, counter)
        timer?.cancel()
        timer = null
    }
}

 class TestCollback : TextCollback {
    var str = ""
    override fun updateString(text: String) {
        str = text
    }
}

 class TestDatasource : DataSource {
    var int = Int.MIN_VALUE
    override fun saveInt(key: String, value: Int) {
        int = value
    }

    override fun getInt(key: String) = int

}


