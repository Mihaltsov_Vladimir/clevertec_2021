package com.example.task_1

class MyViewModel(private val model: Model) {

    private var textObserver: TextObserveble? = null

    private val collback = object : TextCollback {
        override fun updateString(text: String) {
            textObserver?.postValue(text)
        }
    }

    fun init(textObserveble: TextObserveble) {
        this.textObserver = textObserveble
    }

    fun clear() {
        textObserver = null
    }

    fun resumeCounter() {
        model.start(collback)
    }

    fun pauseCounter() {
        model.stop()
    }
}

class TextObserveble {
    private lateinit var textCollback: TextCollback

    fun observe(textCollback: TextCollback) {
        this.textCollback = textCollback
    }

    fun postValue(text: String) {
        textCollback.updateString(text)
    }

}

interface TextCollback {
    fun updateString(text: String)
}

