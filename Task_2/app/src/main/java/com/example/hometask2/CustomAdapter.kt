package com.example.hometask2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.hometask2.model.FakeClassItem
import java.util.logging.Logger

class CustomAdapter(
    private val dataSet: List<FakeClassItem>,
    listenerPosition: ListenerPosition?) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    private var collback: ListenerPosition? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder, parent, false)
        return ViewHolder(view, collback)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = dataSet[position].title
        holder.description.text = dataSet[position].description
    }

    override fun getItemCount() = dataSet.size

    class ViewHolder(view: View, collback1: ListenerPosition?) :
        RecyclerView.ViewHolder(view) {
        val title: TextView
        val description: TextView

        init {
            view.setOnClickListener { collback1?.onClicked(absoluteAdapterPosition) }
            title = view.findViewById(R.id.title_fr_1)
            description = view.findViewById(R.id.description_fr_1)
        }
    }

    init {
        collback = listenerPosition
    }
}