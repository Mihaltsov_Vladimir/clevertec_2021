package com.example.hometask2

import android.app.Activity
import android.util.Pair as UtilPair

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.navigation.ActivityNavigatorExtras
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.hometask2.model.FakeClassItem
import com.example.hometask2.repository.CacheDataSource

class DetailFragment : Fragment() {

    private var title: TextView? = null
    private var description: TextView? = null
    private var buttonExit: Button? = null
    private var image: ImageView? = null
    private var toolbar: androidx.appcompat.widget.Toolbar? = null


    private val cacheData: List<FakeClassItem> = CacheDataSource().getDetails()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: DetailFragmentArgs by navArgs()
        val position = args.position
        findViews(view)
        title?.text = cacheData[position].title
        description?.text = cacheData[position].description
        image?.scaleX=image!!.scaleX
        image?.scaleY=image!!.scaleY
        buttonExit?.setOnClickListener { activity!!.finish() }
        (activity as AppCompatActivity).supportActionBar?.title = "Details"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar?.setNavigationOnClickListener {
            DetailFragmentDirections.actionDetailFragmentToRecyclerViewFragment().let {
                view.findNavController().navigate(it)
            }
        }
    }

    private fun findViews(view: View) {
        image = view.findViewById(R.id.image2)
        title = view.findViewById(R.id.title_fr_2)
        description = view.findViewById(R.id.description_fr_2)
        buttonExit = view.findViewById(R.id.button_end);
        toolbar = (activity as AppCompatActivity).findViewById(R.id.tool_bar)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }
}