package com.example.hometask2

interface ListenerPosition {

    fun onClicked(number: Int)

}