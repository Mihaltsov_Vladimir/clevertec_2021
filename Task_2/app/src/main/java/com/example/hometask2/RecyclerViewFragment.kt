package com.example.hometask2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hometask2.model.FakeClassItem
import com.example.hometask2.repository.CacheDataSource


class RecyclerViewFragment : Fragment() {

    private val cacheData: List<FakeClassItem> by lazy {
        CacheDataSource().getDetails()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<RecyclerView>(R.id.recycler_view).apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = CustomAdapter(cacheData, collback)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_recycler_view, container, false)

    private val collback = object : ListenerPosition {
        override fun onClicked(number: Int) {
            RecyclerViewFragmentDirections.actionRecyclerVToDetailFragment().let {
                it.position = number
                view?.findNavController()?.navigate(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
    }
}

