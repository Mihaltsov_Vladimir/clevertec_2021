package com.example.hometask2.model

class CreateNewFakeObject : CreateOneItem {

    override fun createDummyItem(position: Int): FakeClassItem {

        return FakeClassItem(
            position,
            "Title: $position",
            "Description: $position"
        )
    }
}