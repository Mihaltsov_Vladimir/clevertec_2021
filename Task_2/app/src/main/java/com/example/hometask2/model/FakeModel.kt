package com.example.hometask2.model

class FakeModel(private val createOneItem: CreateOneItem) : FakeItem {

    private  val fakeData: MutableList<FakeClassItem> by lazy {
        ArrayList()
    }

    override fun createListItem()= fakeData.also{
        for (i in 1..1000) it.add(createOneItem.createDummyItem(i))
    }
}