package com.example.hometask2.repository

import com.example.hometask2.model.FakeClassItem
import com.example.hometask2.model.FakeModel
import com.example.hometask2.model.CreateNewFakeObject
import com.example.hometask2.model.CreateOneItem

class CacheDataSource() : DataSource {


    private val fakeModel: FakeModel by lazy {
        FakeModel(CreateNewFakeObject())
    }

    private val cacheDataSource: MutableList<FakeClassItem> by lazy {
        ArrayList()
    }

    override fun getDetails() = cacheDataSource.apply {
        addAll(fakeModel.createListItem())
    }
}

