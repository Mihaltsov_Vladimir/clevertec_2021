package com.example.hometask2.repository

import com.example.hometask2.model.FakeClassItem

interface DataSource {

    fun getDetails(): List<FakeClassItem>

}