package com.example.task_3.fragments

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.example.task_3.R

public class ContactFragmentDirections private constructor() {
  public companion object {
    public fun actionContactFragmentToMainFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_contactFragment_to_mainFragment)
  }
}
