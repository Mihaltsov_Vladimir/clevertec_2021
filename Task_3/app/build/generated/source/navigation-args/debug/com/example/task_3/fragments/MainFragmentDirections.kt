package com.example.task_3.fragments

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.example.task_3.R

public class MainFragmentDirections private constructor() {
  public companion object {
    public fun actionMainFragmentToContactFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_mainFragment_to_contactFragment)
  }
}
