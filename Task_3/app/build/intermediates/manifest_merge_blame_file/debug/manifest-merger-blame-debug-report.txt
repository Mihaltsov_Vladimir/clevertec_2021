1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.example.task_3"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="21"
8-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="31" />
9-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.READ_CONTACTS" >
11-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:4:5-6:23
11-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:4:22-69
12    </uses-permission>
13
14    <!-- Permission will be merged into the manifest of the hosting app. -->
15    <!-- Is required to launch foreground extraction service for targetSdkVersion 28+. -->
16    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
16-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:8:3-74
16-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:8:20-72
17
18    <application
18-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:8:5-24:19
19        android:allowBackup="true"
19-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:9:9-35
20        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
20-->[androidx.core:core:1.6.0] /Users/sonya/.gradle/caches/transforms-3/c690eb671f68fe48d2360ac103675572/transformed/core-1.6.0/AndroidManifest.xml:24:18-86
21        android:debuggable="true"
22        android:icon="@mipmap/ic_launcher"
22-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:10:9-43
23        android:label="@string/app_name"
23-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:11:9-41
24        android:roundIcon="@mipmap/ic_launcher_round"
24-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:12:9-54
25        android:supportsRtl="true"
25-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:13:9-35
26        android:testOnly="true"
27        android:theme="@style/Theme.Task_3" >
27-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:14:9-44
28        <activity
28-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:15:9-23:20
29            android:name="com.example.task_3.MainActivity"
29-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:16:13-41
30            android:exported="true" >
30-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:17:13-36
31            <intent-filter>
31-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:18:13-22:29
32                <action android:name="android.intent.action.MAIN" />
32-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:19:17-69
32-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:19:25-66
33
34                <category android:name="android.intent.category.LAUNCHER" />
34-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:21:17-77
34-->/Users/sonya/Desktop/MyRepo/clevertec_2021/clevertec_2021/Task_3/app/src/main/AndroidManifest.xml:21:27-74
35            </intent-filter>
36        </activity>
37
38        <service
38-->[androidx.room:room-runtime:2.4.0-alpha04] /Users/sonya/.gradle/caches/transforms-3/0c0ce8839bb9274a6efc296a1660494e/transformed/room-runtime-2.4.0-alpha04/AndroidManifest.xml:25:9-28:40
39            android:name="androidx.room.MultiInstanceInvalidationService"
39-->[androidx.room:room-runtime:2.4.0-alpha04] /Users/sonya/.gradle/caches/transforms-3/0c0ce8839bb9274a6efc296a1660494e/transformed/room-runtime-2.4.0-alpha04/AndroidManifest.xml:26:13-74
40            android:directBootAware="true"
40-->[androidx.room:room-runtime:2.4.0-alpha04] /Users/sonya/.gradle/caches/transforms-3/0c0ce8839bb9274a6efc296a1660494e/transformed/room-runtime-2.4.0-alpha04/AndroidManifest.xml:27:13-43
41            android:exported="false" />
41-->[androidx.room:room-runtime:2.4.0-alpha04] /Users/sonya/.gradle/caches/transforms-3/0c0ce8839bb9274a6efc296a1660494e/transformed/room-runtime-2.4.0-alpha04/AndroidManifest.xml:28:13-37
42
43        <provider
43-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/sonya/.gradle/caches/transforms-3/d6f7cc618479bb3909d17a36c679649c/transformed/jetified-lifecycle-process-2.2.0/AndroidManifest.xml:25:9-29:43
44            android:name="androidx.lifecycle.ProcessLifecycleOwnerInitializer"
44-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/sonya/.gradle/caches/transforms-3/d6f7cc618479bb3909d17a36c679649c/transformed/jetified-lifecycle-process-2.2.0/AndroidManifest.xml:26:13-79
45            android:authorities="com.example.task_3.lifecycle-process"
45-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/sonya/.gradle/caches/transforms-3/d6f7cc618479bb3909d17a36c679649c/transformed/jetified-lifecycle-process-2.2.0/AndroidManifest.xml:27:13-69
46            android:exported="false"
46-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/sonya/.gradle/caches/transforms-3/d6f7cc618479bb3909d17a36c679649c/transformed/jetified-lifecycle-process-2.2.0/AndroidManifest.xml:28:13-37
47            android:multiprocess="true" />
47-->[androidx.lifecycle:lifecycle-process:2.2.0] /Users/sonya/.gradle/caches/transforms-3/d6f7cc618479bb3909d17a36c679649c/transformed/jetified-lifecycle-process-2.2.0/AndroidManifest.xml:29:13-40
48        <provider
48-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:26:9-34:20
49            android:name="androidx.startup.InitializationProvider"
49-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:27:13-67
50            android:authorities="com.example.task_3.androidx-startup"
50-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:28:13-68
51            android:exported="false" >
51-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:29:13-37
52            <meta-data
52-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:31:13-33:52
53                android:name="androidx.profileinstaller.ProfileInstallerInitializer"
53-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:32:17-85
54                android:value="androidx.startup" />
54-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:33:17-49
55        </provider>
56
57        <receiver
57-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:36:9-45:20
58            android:name="androidx.profileinstaller.ProfileInstallReceiver"
58-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:37:13-76
59            android:directBootAware="false"
59-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:38:13-44
60            android:enabled="true"
60-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:39:13-35
61            android:exported="true"
61-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:40:13-36
62            android:permission="android.permission.DUMP" >
62-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:41:13-57
63            <intent-filter>
63-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:42:13-44:29
64                <action android:name="androidx.profileinstaller.action.INSTALL_PROFILE" />
64-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:43:17-91
64-->[androidx.profileinstaller:profileinstaller:1.0.1] /Users/sonya/.gradle/caches/transforms-3/aedc18a7f23ced80cbbb733daa902c35/transformed/jetified-profileinstaller-1.0.1/AndroidManifest.xml:43:25-88
65            </intent-filter>
66        </receiver> <!-- The activities will be merged into the manifest of the hosting app. -->
67        <activity
67-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:12:5-271
68            android:name="com.google.android.play.core.missingsplits.PlayCoreMissingSplitsActivity"
68-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:12:15-102
69            android:enabled="false"
69-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:12:103-126
70            android:exported="false"
70-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:12:127-151
71            android:launchMode="singleInstance"
71-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:12:204-239
72            android:process=":playcore_missing_splits_activity"
72-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:12:152-203
73            android:stateNotNeeded="true" />
73-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:12:240-269
74        <activity
74-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:13:5-226
75            android:name="com.google.android.play.core.common.PlayCoreDialogWrapperActivity"
75-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:13:15-95
76            android:enabled="false"
76-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:13:96-119
77            android:exported="false"
77-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:13:120-144
78            android:stateNotNeeded="true"
78-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:13:145-174
79            android:theme="@style/Theme.PlayCore.Transparent" /> <!-- The services will be merged into the manifest of the hosting app. -->
79-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:13:175-224
80        <service
80-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:16:5-18:15
81            android:name="com.google.android.play.core.assetpacks.AssetPackExtractionService"
81-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:16:14-95
82            android:enabled="false"
82-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:16:96-119
83            android:exported="true" >
83-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:16:120-143
84            <meta-data
84-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:17:7-108
85                android:name="com.google.android.play.core.assetpacks.versionCode"
85-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:17:18-84
86                android:value="10901" />
86-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:17:85-106
87        </service>
88        <service
88-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:19:5-147
89            android:name="com.google.android.play.core.assetpacks.ExtractionForegroundService"
89-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:19:14-96
90            android:enabled="false"
90-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:19:97-120
91            android:exported="false" />
91-->[com.google.android.play:core:1.9.1] /Users/sonya/.gradle/caches/transforms-3/9f46bf059a569b14d0ad11a45946f74b/transformed/jetified-core-1.9.1/AndroidManifest.xml:19:121-145
92    </application>
93
94</manifest>
