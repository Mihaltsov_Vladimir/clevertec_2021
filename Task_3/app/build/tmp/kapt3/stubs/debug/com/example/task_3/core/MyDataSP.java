package com.example.task_3.core;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nJ\u0016\u0010\f\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/example/task_3/core/MyDataSP;", "", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "sharedPref", "Landroid/content/SharedPreferences;", "addNumberToSP", "", "myKey", "", "text", "readNumberFromSP", "app_debug"})
public final class MyDataSP {
    private final android.app.Activity activity = null;
    private android.content.SharedPreferences sharedPref;
    
    public MyDataSP(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
        super();
    }
    
    public final void addNumberToSP(@org.jetbrains.annotations.NotNull()
    java.lang.String myKey, @org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String readNumberFromSP(@org.jetbrains.annotations.NotNull()
    java.lang.String myKey, @org.jetbrains.annotations.NotNull()
    java.lang.String text) {
        return null;
    }
}