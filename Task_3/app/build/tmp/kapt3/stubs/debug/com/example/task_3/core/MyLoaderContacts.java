package com.example.task_3.core;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\f\u001a\u0012\u0012\u0004\u0012\u00020\u000e0\rj\b\u0012\u0004\u0012\u00020\u000e`\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\n8\u0002X\u0083\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000b\u00a8\u0006\u0010"}, d2 = {"Lcom/example/task_3/core/MyLoaderContacts;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "cursor", "Landroid/database/Cursor;", "displayName", "", "myProgection", "", "[Ljava/lang/String;", "getContactList", "Ljava/util/ArrayList;", "Lcom/example/task_3/room/ContactModel;", "Lkotlin/collections/ArrayList;", "app_debug"})
public final class MyLoaderContacts {
    private final android.content.Context context = null;
    private final java.lang.String displayName = null;
    @android.annotation.SuppressLint(value = {"InlinedApi"})
    private final java.lang.String[] myProgection = null;
    private final android.database.Cursor cursor = null;
    
    public MyLoaderContacts(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.example.task_3.room.ContactModel> getContactList() {
        return null;
    }
}