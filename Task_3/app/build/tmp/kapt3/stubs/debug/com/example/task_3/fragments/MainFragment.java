package com.example.task_3.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0018\u001a\u00020\u0019H\u0002J$\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J\u001a\u0010\"\u001a\u00020\u00192\u0006\u0010#\u001a\u00020\u001b2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J\u0010\u0010$\u001a\u00020\u00192\u0006\u0010%\u001a\u00020\bH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00110\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/example/task_3/fragments/MainFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/example/task_3/databinding/FragmentMainBinding;", "activityResultLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "", "", "allContactsPhone", "", "Lcom/example/task_3/room/ContactModel;", "binding", "getBinding", "()Lcom/example/task_3/databinding/FragmentMainBinding;", "listContactsFromRoom", "listContactsFromRoomRofDialog", "", "listenerMyDialog", "Lcom/example/task_3/core/DialogListener;", "myDataSP", "Lcom/example/task_3/core/MyDataSP;", "viewModel", "Lcom/example/task_3/ContactViewModel;", "createNotificationChannel", "", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "showSnackBar", "someText", "app_debug"})
public final class MainFragment extends androidx.fragment.app.Fragment {
    private com.example.task_3.databinding.FragmentMainBinding _binding;
    private com.example.task_3.core.MyDataSP myDataSP;
    private com.example.task_3.ContactViewModel viewModel;
    private java.util.List<com.example.task_3.room.ContactModel> allContactsPhone;
    private java.util.List<com.example.task_3.room.ContactModel> listContactsFromRoom;
    private java.util.List<java.util.List<com.example.task_3.room.ContactModel>> listContactsFromRoomRofDialog;
    private final androidx.activity.result.ActivityResultLauncher<java.lang.String[]> activityResultLauncher = null;
    private final com.example.task_3.core.DialogListener listenerMyDialog = null;
    private java.util.HashMap _$_findViewCache;
    
    public MainFragment() {
        super();
    }
    
    private final com.example.task_3.databinding.FragmentMainBinding getBinding() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void createNotificationChannel() {
    }
    
    private final void showSnackBar(java.lang.String someText) {
    }
}