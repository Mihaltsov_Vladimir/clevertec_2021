package com.example.task_3.fragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B!\u0012\u0012\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0012\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016R$\u0010\t\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\nX\u0086.\u00a2\u0006\u0010\n\u0002\u0010\u0010\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/example/task_3/fragments/MyDialog;", "Landroidx/appcompat/app/AppCompatDialogFragment;", "list", "", "", "Lcom/example/task_3/room/ContactModel;", "dialogListener", "Lcom/example/task_3/core/DialogListener;", "(Ljava/util/List;Lcom/example/task_3/core/DialogListener;)V", "array", "", "", "getArray", "()[Ljava/lang/String;", "setArray", "([Ljava/lang/String;)V", "[Ljava/lang/String;", "listener", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class MyDialog extends androidx.appcompat.app.AppCompatDialogFragment {
    private final java.util.List<java.util.List<com.example.task_3.room.ContactModel>> list = null;
    private final com.example.task_3.core.DialogListener dialogListener = null;
    private com.example.task_3.core.DialogListener listener;
    public java.lang.String[] array;
    private java.util.HashMap _$_findViewCache;
    
    public MyDialog(@org.jetbrains.annotations.NotNull()
    java.util.List<java.util.List<com.example.task_3.room.ContactModel>> list, @org.jetbrains.annotations.NotNull()
    com.example.task_3.core.DialogListener dialogListener) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String[] getArray() {
        return null;
    }
    
    public final void setArray(@org.jetbrains.annotations.NotNull()
    java.lang.String[] p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.app.Dialog onCreateDialog(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
}