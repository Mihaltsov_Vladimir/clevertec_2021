package com.example.task_3.room;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\bg\u0018\u00002\u00020\u0001J%\u0010\u0002\u001a\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007J\u0019\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u000bH\'J\u0014\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\r0\u000bH\'\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000e"}, d2 = {"Lcom/example/task_3/room/ContactDao;", "", "addContact", "", "contactModel", "", "Lcom/example/task_3/room/ContactModel;", "([Lcom/example/task_3/room/ContactModel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "delete", "(Lcom/example/task_3/room/ContactModel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getAll", "Landroidx/lifecycle/LiveData;", "readAll", "", "app_debug"})
public abstract interface ContactDao {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM contact_my_phone")
    public abstract androidx.lifecycle.LiveData<com.example.task_3.room.ContactModel> getAll();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM contact_my_phone ORDER BY contactId ASC")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.example.task_3.room.ContactModel>> readAll();
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Insert(entity = com.example.task_3.room.ContactModel.class, onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract java.lang.Object addContact(@org.jetbrains.annotations.NotNull()
    com.example.task_3.room.ContactModel[] contactModel, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Delete(entity = com.example.task_3.room.ContactModel.class)
    public abstract java.lang.Object delete(@org.jetbrains.annotations.NotNull()
    com.example.task_3.room.ContactModel contactModel, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
}