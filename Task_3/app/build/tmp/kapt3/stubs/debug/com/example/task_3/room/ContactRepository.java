package com.example.task_3.room;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0007H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u001d\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u000b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/example/task_3/room/ContactRepository;", "", "contactDao", "Lcom/example/task_3/room/ContactDao;", "(Lcom/example/task_3/room/ContactDao;)V", "readAllContacts", "Landroidx/lifecycle/LiveData;", "Lcom/example/task_3/room/ContactModel;", "getReadAllContacts", "()Landroidx/lifecycle/LiveData;", "readAllContactsToDialog", "", "getReadAllContactsToDialog", "addContact", "", "contactModel", "(Lcom/example/task_3/room/ContactModel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class ContactRepository {
    private final com.example.task_3.room.ContactDao contactDao = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<com.example.task_3.room.ContactModel> readAllContacts = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<com.example.task_3.room.ContactModel>> readAllContactsToDialog = null;
    
    public ContactRepository(@org.jetbrains.annotations.NotNull()
    com.example.task_3.room.ContactDao contactDao) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.task_3.room.ContactModel> getReadAllContacts() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.task_3.room.ContactModel>> getReadAllContactsToDialog() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object addContact(@org.jetbrains.annotations.NotNull()
    com.example.task_3.room.ContactModel contactModel, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
}