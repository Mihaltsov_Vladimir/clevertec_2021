package com.example.task_3

import android.app.Application
import androidx.lifecycle.*
import com.example.task_3.room.ContactModel
import com.example.task_3.room.ContactRepository
import com.example.task_3.room.ContactRoomDataBase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ContactViewModel(application: Application) : AndroidViewModel(application) {

     val readAllData: LiveData<ContactModel>
     val readAllDatatoDialog : LiveData<List<ContactModel>>

    private val repository: ContactRepository

    fun addContact(contactModel: ContactModel) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.addContact(contactModel)
        }
    }

    init {
        val contactDao = ContactRoomDataBase.getDatabase(application).contactDao()
        repository = ContactRepository(contactDao)

        readAllData = repository.readAllContacts
        readAllDatatoDialog=repository.readAllContactsToDialog
    }



}