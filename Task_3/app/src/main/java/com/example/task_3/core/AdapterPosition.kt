package com.example.task_3.core

interface AdapterPosition {
    fun onClicked(position: Int)
}
