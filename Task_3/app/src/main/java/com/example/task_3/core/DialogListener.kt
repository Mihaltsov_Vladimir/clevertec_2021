package com.example.task_3.core

interface DialogListener {
    fun onDialogClick(position: Int)
}