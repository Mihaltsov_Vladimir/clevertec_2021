package com.example.task_3.core

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

class MyDataSP(private val activity: Activity) {

    private var sharedPref: SharedPreferences = activity.getPreferences(Context.MODE_PRIVATE)

    fun addNumberToSP(myKey: String, text: String) {
        val sharedPref = activity.getPreferences(Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) { putString(myKey, text).apply() }
    }

    fun readNumberFromSP(myKey: String, text: String): String =
        sharedPref.getString(myKey, text).toString()
}


