package com.example.task_3.core

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.provider.ContactsContract
import android.text.TextUtils
import android.util.Patterns
import com.example.task_3.room.ContactModel

class MyLoaderContacts(private val context: Context) {

    private val displayName =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
        else ContactsContract.Contacts.DISPLAY_NAME

    @SuppressLint("InlinedApi")
    private val myProgection = arrayOf(
        ContactsContract.Contacts._ID,
        displayName,
        ContactsContract.Contacts.HAS_PHONE_NUMBER
    )
    private val cursor = context.contentResolver?.query(
        ContactsContract.Contacts.CONTENT_URI,
        myProgection,
        null,
        null,
        null
    )

    fun getContactList(): ArrayList<ContactModel> {
        val contacts: ArrayList<ContactModel> = ArrayList()

        var counterId = 0
        val idInx = cursor?.getColumnIndex(ContactsContract.Contacts._ID)
        val nameInx = cursor?.getColumnIndex(displayName)
        val hasPhoneInx = cursor?.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)
        if (cursor != null && cursor.moveToFirst()) {

            do {
                val id = cursor.getString(idInx!!)
                val name = cursor.getString(nameInx!!)
                counterId++
                var phone = ""
                var email = ""
                val hasPhone = cursor.getInt(hasPhoneInx!!)

                val contentResolverEmail = context.contentResolver?.query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                    arrayOf(id),
                    null
                )

                val emailInx =
                    contentResolverEmail?.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)
                if (contentResolverEmail != null && contentResolverEmail.moveToFirst()) {
                    email =
                        contentResolverEmail.getString(emailInx!!)
                    contentResolverEmail.close()
                }

                if (hasPhone > 0) {

                    val contentResolverPhone = context.contentResolver?.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )

                    val phoneInx =
                        contentResolverPhone?.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)

                    if (contentResolverPhone != null && contentResolverPhone.moveToFirst()) {
                        phone = contentResolverPhone.getString(phoneInx!!)
                        contentResolverPhone.close()
                    }
                }

                if ((!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
                            && !email.equals(name, ignoreCase = true)) || !TextUtils.isEmpty(phone)
                ) {

                    val contact = ContactModel(counterId, phone, name, email)
                    contacts.add(contact)

                }
            } while (cursor.moveToNext())

            cursor.close()
        }

        return contacts
    }
}