package com.example.task_3.core

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.task_3.R

class MyNotification(private val text: String,private val context: Context) {

    fun createNotification() {
        val builder = NotificationCompat.Builder(context,context.getString(R.string.CHANNEL_ID))
            .setSmallIcon(R.drawable.ic_baseline_person_24)
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        with(NotificationManagerCompat.from(context)) {
            notify(1, builder.build())
        }
    }
}