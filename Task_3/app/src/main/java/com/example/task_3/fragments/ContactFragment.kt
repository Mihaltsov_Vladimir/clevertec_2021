package com.example.task_3.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.task_3.ContactViewModel
import com.example.task_3.R
import com.example.task_3.core.AdapterPosition
import com.example.task_3.core.MyLoaderContacts
import com.example.task_3.databinding.FragmentContactBinding
import com.example.task_3.room.ContactModel

class ContactFragment : Fragment() {

    private var _binding: FragmentContactBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: ContactViewModel
    private lateinit var allContactsPhone: MutableList<ContactModel>
    private lateinit var saveContacted: MutableList<ContactModel>


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<RecyclerView>(R.id.recycler_view).apply {
            MyLoaderContacts(binding.root.context).getContactList()
                .forEach { allContactsPhone.add(it) }
            layoutManager = LinearLayoutManager(activity)
            adapter = MyAdapter(allContactsPhone, adapterPosition)

        }
    }

    private val adapterPosition = object : AdapterPosition {
        override fun onClicked(position: Int) {

            viewModel.addContact(allContactsPhone[position])

            Toast.makeText(activity, resources.getString(R.string.save_ok), Toast.LENGTH_LONG)
                .show()
            ContactFragmentDirections.actionContactFragmentToMainFragment().let {
                findNavController().navigate(it)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        allContactsPhone = mutableListOf()
        saveContacted = mutableListOf()
        viewModel = ViewModelProvider(this)[ContactViewModel::class.java]
        _binding = FragmentContactBinding.inflate(inflater, container, false)
        return binding.root
    }

}
