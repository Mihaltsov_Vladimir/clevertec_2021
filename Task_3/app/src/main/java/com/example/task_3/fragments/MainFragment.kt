package com.example.task_3.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.task_3.ContactViewModel
import com.example.task_3.R
import com.example.task_3.core.DialogListener
import com.example.task_3.core.MyDataSP
import com.example.task_3.core.MyNotification
import com.example.task_3.databinding.FragmentMainBinding
import com.example.task_3.room.ContactModel
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_main.*


class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private var myDataSP: MyDataSP? = null
    private lateinit var viewModel: ContactViewModel

    private lateinit var allContactsPhone: MutableList<ContactModel>
    private lateinit var listContactsFromRoom: MutableList<ContactModel>
    private lateinit var listContactsFromRoomRofDialog: MutableList<List<ContactModel>>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.contactInPhone.setOnClickListener {
            when (PackageManager.PERMISSION_GRANTED) {
                ContextCompat.checkSelfPermission(it.context, Manifest.permission.READ_CONTACTS)
                -> {
                    MainFragmentDirections.actionMainFragmentToContactFragment().let {
                        view.findNavController().navigate(it)
                    }
                }
                else -> activityResultLauncher.launch(arrayOf(Manifest.permission.READ_CONTACTS))
            }
        }

        binding.contactInRoom.setOnClickListener {
            if (listContactsFromRoom.isNotEmpty())
                MyDialog(listContactsFromRoomRofDialog, listenerMyDialog).show(
                    childFragmentManager,
                    null
                )
            else Toast.makeText(
                activity,
                resources.getString(R.string.toast_room_negative),
                Toast.LENGTH_LONG
            )
                .show()
        }



        binding.contactInSp.setOnClickListener {
            val default = myDataSP?.readNumberFromSP(
                resources.getString(R.string.key_sp),
                resources.getString(R.string.saved_high_score_default_key)
            ).toString()
            binding.sharedPreferencesResult.text =
                myDataSP?.readNumberFromSP(resources.getString(R.string.key_sp), default).toString()
            showSnackBar(
                myDataSP?.readNumberFromSP(resources.getString(R.string.key_sp), default).toString()
            )
        }

        binding.showNotification.setOnClickListener {

            var temp =
                myDataSP?.readNumberFromSP(resources.getString(R.string.key_sp), "").toString()
            listContactsFromRoom.forEach {
                temp = if (it.number == temp) it.name.toString()
                else myDataSP?.readNumberFromSP(
                    resources.getString(R.string.key_sp),
                    resources.getString(R.string.notification_abd_room)
                ).toString()
            }

            MyNotification(temp, it.context).createNotification()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        listContactsFromRoom = mutableListOf()
        allContactsPhone = mutableListOf()
        listContactsFromRoomRofDialog = mutableListOf()

        viewModel = ViewModelProvider(this)[ContactViewModel::class.java]
        viewModel.readAllData.observe(viewLifecycleOwner) { user ->
            if (user != null)
                listContactsFromRoom.add(user)
        }
        viewModel.readAllDatatoDialog.observe(viewLifecycleOwner) { user ->
            listContactsFromRoomRofDialog.add(user)
        }

        createNotificationChannel()
        myDataSP = MyDataSP(requireActivity())
        _binding = FragmentMainBinding.inflate(inflater, container, false)

        return binding.root

    }


    private val activityResultLauncher: ActivityResultLauncher<Array<String>> =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions())
        { result ->
            var allAreGranted = true
            for (b in result.values) allAreGranted = allAreGranted && b
            if (allAreGranted) {
                MainFragmentDirections.actionMainFragmentToContactFragment().let {
                    view?.findNavController()?.navigate(it)
                }
            }

        }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(
                resources.getString(R.string.CHANNEL_ID),
                name,
                importance
            ).apply {
                description = descriptionText
            }
            val manager = requireContext().getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channel)
        }
    }

    private val listenerMyDialog = object : DialogListener {
        @SuppressLint("SetTextI18n")
        override fun onDialogClick(position: Int) {

            myDataSP?.addNumberToSP(
                resources.getString(R.string.key_sp),
                listContactsFromRoomRofDialog[0][position].number
            )
            binding.sharedPreferencesResult.text = "Name ${listContactsFromRoomRofDialog[0][position].name} " +
                    "\n Number ${listContactsFromRoomRofDialog[0][position].number}" +
                    "\n Email ${listContactsFromRoomRofDialog[0][position].email}"

            Toast.makeText(
                activity,
                "Name ${listContactsFromRoomRofDialog[0][position].name}  " +
                        "\n number ${listContactsFromRoomRofDialog[0][position].number} " +
                        "\n" +
                        " email ${listContactsFromRoomRofDialog[0][position].email}",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun showSnackBar(someText: String) {
        val mySnackbar =
            Snackbar.make(binding.root, someText, BaseTransientBottomBar.LENGTH_INDEFINITE)
        mySnackbar.setAction(resources.getString(R.string.my_snackbar_action_ok))
        { mySnackbar.dismiss() }.show()
    }

}
