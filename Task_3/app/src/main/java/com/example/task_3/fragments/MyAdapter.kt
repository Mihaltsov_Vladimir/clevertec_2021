package com.example.task_3.fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.task_3.R
import com.example.task_3.core.AdapterPosition
import com.example.task_3.room.ContactModel

class MyAdapter(
    private val dataSet: MutableList<ContactModel>,
    private val adapterPosition: AdapterPosition
) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    private var adapter: AdapterPosition? = null

    class ViewHolder(itemView: View, adapter: AdapterPosition?) :
        RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.tv_name)
        val number: TextView = itemView.findViewById(R.id.tv_number)

        init {
            itemView.setOnClickListener {
                adapter?.onClicked(absoluteAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        adapter = adapterPosition
        return ViewHolder(view, adapter)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: ContactModel = dataSet[position]
        holder.name.text = model.name
        holder.number.text = model.number
    }

    override fun getItemCount() = dataSet.size

}
