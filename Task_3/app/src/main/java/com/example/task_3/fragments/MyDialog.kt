package com.example.task_3.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.DialogFragment
import com.example.task_3.R
import com.example.task_3.core.DialogListener
import com.example.task_3.room.ContactModel


class MyDialog(
    private val list: MutableList<List<ContactModel>>,
    private val dialogListener: DialogListener
) : AppCompatDialogFragment() {

    private var listener: DialogListener? = null
    lateinit var array: Array<String?>

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        listener = dialogListener


        array = list[0].map { it.number }.toTypedArray()
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setItems(array) { dialog, which -> listener?.onDialogClick(which) }
                .setNegativeButton(R.string.cancel) { dialog, id -> dismiss() }
                .setCancelable(false)
                .setTitle(R.string.pick_color)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}




