package com.example.task_3.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ContactDao {

    @Query("SELECT * FROM ${ContactModel.TABLE_NAME}")
     fun getAll(): LiveData<ContactModel>

    @Query("SELECT * FROM ${ContactModel.TABLE_NAME} ORDER BY contactId ASC")
    fun readAll(): LiveData<List<ContactModel>>


    @Insert(entity = ContactModel::class,onConflict = OnConflictStrategy.REPLACE)
    suspend fun addContact( vararg contactModel: ContactModel)


    @Delete(entity = ContactModel::class)
    suspend fun delete(contactModel: ContactModel)

//    @Query("SELECT * FROM ${ContactModel.TABLE_NAME} WHERE number = :number")
//   suspend fun getByNumber(number: String): ContactModel
}
