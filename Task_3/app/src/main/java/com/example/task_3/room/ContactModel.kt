package com.example.task_3.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.task_3.room.ContactModel.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class ContactModel(

@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "contactId") val id: Int = 0,

@ColumnInfo(name = "contactNumber") val number: String,

@ColumnInfo(name = "contactName") val name: String?,

@ColumnInfo(name = "contactIdEmail") val email: String?
){
    companion object{
        const val TABLE_NAME="contact_my_phone"
    }
}