package com.example.task_3.room

import androidx.lifecycle.LiveData

class ContactRepository(private val contactDao: ContactDao) {

    val readAllContacts: LiveData<ContactModel> = contactDao.getAll()

    val readAllContactsToDialog: LiveData<List<ContactModel>> = contactDao.readAll()

   suspend fun addContact(contactModel: ContactModel) = contactDao.addContact(contactModel)

}