package com.example.task4

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.task4.databinding.ActivityMapsBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


//@AndroidEntryPoint
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private val tag = MapsActivity::class.java.simpleName
    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    private val viewModel: MapsViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        val map=HashMap<Int,String>()
        for (i in -1..14){
            map[i] = "item $i"
        }
        map.put(null.hashCode()," nulL @!!!!!!!!!!! ${null.hashCode()}")

        Log.e(" "," map size ${map.size}")


    }

    private val activityResultLauncher: ActivityResultLauncher<Array<String>> =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions())
        { result ->
            var allAreGranted = true
            for (b in result.values) allAreGranted = allAreGranted && b
            if (allAreGranted) {
                getPoints()
            }
        }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val cameraPosition =
            CameraPosition.Builder().target(LatLng(52.425163, 31.015039))
                .zoom(12.0f).build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        mMap.isTrafficEnabled = true

        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            -> {
                getPoints()
            }
            else -> activityResultLauncher.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))
        }
    }


    private fun getPoints() {
        viewModel.liveData().observe(this) { int ->
            int.forEach {
                mMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(it.gpsX, it.gpsY))

                )

                Log.e(tag, "address: ${it.address}  ")
            }
        }
    }
}