package com.example.task4

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task4.model.AtmModelDTO
import com.example.task4.retrofit.AtmService
import com.example.task4.retrofit.RetrofitCreator
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

//@HiltViewModel
class MapsViewModel() : ViewModel() {

    private val tag = MapsActivity::class.java.simpleName
    private val atmService: AtmService = RetrofitCreator.atmService
    private val disposable: Disposable = loadLatLng()

    private val mutableLiveData: MutableLiveData<List<AtmModelDTO>> by lazy {
        MutableLiveData<List<AtmModelDTO>>().also {
            loadLatLng()
        }
    }

    fun liveData(): LiveData<List<AtmModelDTO>> {
        return mutableLiveData
    }

    override fun onCleared() {
        disposable.dispose()
        super.onCleared()

    }


    private fun loadLatLng(): Disposable = cloudDataSource()
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({

            mutableLiveData.postValue(it)
        }, {

        })


    private fun cloudDataSource(): Single<List<AtmModelDTO>> {
        return atmService.getAtm("гомель")
    }
}



