package com.example.task4.model

import com.google.gson.annotations.SerializedName

data class AtmModelDTO(

    @SerializedName("city")
    private var city: String,

    @SerializedName("gps_x")
    var gpsX: Double,

    @SerializedName("gps_y")
    var gpsY: Double,

    @SerializedName("address")
    var address: String
)