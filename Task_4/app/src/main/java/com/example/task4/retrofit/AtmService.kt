package com.example.task4.retrofit

import com.example.task4.model.AtmModelDTO
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AtmService {

    @GET("atm")
    fun getAtm(@Query("city") city: String): Single<List<AtmModelDTO>>

}