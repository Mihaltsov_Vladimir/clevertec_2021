package com.example.task4.retrofit

private const val baseUrl = "https://belarusbank.by/api/"

object RetrofitCreator {

    val atmService: AtmService = RetrofitClient.getClient(baseUrl).create(AtmService::class.java)

}