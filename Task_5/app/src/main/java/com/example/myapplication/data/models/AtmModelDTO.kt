package com.example.myapplication.data.models

import com.google.gson.annotations.SerializedName

data class AtmModelDTO(

    @SerializedName("city")
     var city: String,

    @SerializedName("gps_x")
    var gpsX: Double,

    @SerializedName("gps_y")
    var gpsY: Double,

    @SerializedName("address")
    var address: String
)