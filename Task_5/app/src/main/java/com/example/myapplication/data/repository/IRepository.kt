package com.example.myapplication.data.repository

import com.example.myapplication.domain.PointModel
import io.reactivex.rxjava3.core.Single

interface IRepository {

    fun getAtm(city: String): Single<List<PointModel>>

}
