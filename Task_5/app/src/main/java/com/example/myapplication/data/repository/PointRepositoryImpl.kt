package com.example.myapplication.data.repository

import com.example.myapplication.domain.PointModel
import com.example.myapplication.domain.PointModelType
import com.example.myapplication.data.services.BankApiService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PointRepositoryImpl @Inject constructor(
    private val bankApiService: BankApiService
) : IRepository {

    override fun getAtm(city: String): Single<List<PointModel>> {
        return bankApiService.getAtm(city)
            .map { points ->
                val models: MutableList<PointModel> = mutableListOf()
                for (point in points) {
                    models.add(
                        PointModel(
                            PointModelType.ATM.type,
                            point.gpsX,
                            point.gpsY,
                        )
                    )
                }
                return@map models
            }
    }

    fun getInfo(city: String): Single<List<PointModel>> {
        return bankApiService.getInfoBox(city)
            .map { points ->
                val models: MutableList<PointModel> = mutableListOf()
                for (point in points) {
                    models.add(
                        PointModel(
                            PointModelType.INFOBOX.type,
                            point.gpsX,
                            point.gpsY,
                        )
                    )
                }
                return@map models
            }
    }

    fun getFilials(city: String): Single<List<PointModel>> {
        return bankApiService.getFilial(city)
            .map { points ->
                val models: MutableList<PointModel> = mutableListOf()
                for (point in points) {
                    models.add(
                        PointModel(
                            PointModelType.FILIAL.type,
                            point.gpsX,
                            point.gpsY,
                        )
                    )
                }
                return@map models
            }
    }

}
