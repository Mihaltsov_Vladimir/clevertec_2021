package com.example.myapplication.data.services

import com.example.myapplication.data.models.AtmModelDTO
import com.example.myapplication.data.models.FilialsModelDTO
import com.example.myapplication.data.models.InfoboxModelDTO
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BankApiService {

    @GET("atm")
    fun getAtm(@Query("city") city: String): Single<List<AtmModelDTO>>

    @GET("filials_info")
    fun getFilial(@Query("city") city: String): Single<List<FilialsModelDTO>>

    @GET("infobox")
    fun getInfoBox(@Query("city") city: String): Single<List<InfoboxModelDTO>>
}