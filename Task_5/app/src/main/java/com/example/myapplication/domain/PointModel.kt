package com.example.myapplication.domain


class PointModel(
    var type: String,
    var gpsX: Double,
    var gpsY: Double,
)

enum class PointModelType(val type:String) {
    ATM("ATM"),
    INFOBOX("InfoBox"),
    FILIAL("Filial")
}
