package com.example.myapplication.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityMapsBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.android.AndroidInjection
import javax.inject.Inject

private val TAG = MapsActivity::class.java.simpleName
private val nullPoint = LatLng(52.425163, 31.015039)
private val city="гомель"

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var mapsViewModel: MapsViewModel
    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)
        mapsViewModel = ViewModelProvider(this, viewModelFactory)[MapsViewModel::class.java]
        mapsViewModel.loadLatLng(city)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        mapsViewModel.liveDataStatus().observe(this) {
            Toast.makeText(this, "Произошла ошибка -$it", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val cameraPosition =
            CameraPosition.Builder().target(nullPoint)
                .zoom(14.0f).build()
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        mMap.isTrafficEnabled = true
        getPoints()
    }


    private fun getPoints() {
        mapsViewModel.liveDataPoints(nullPoint).observe(this) { int ->
            int.forEach {
                mMap.addMarker(
                    MarkerOptions()
                        .position(LatLng(it.gpsX, it.gpsY))
                        .title(it.type)
                )
            }
        }
    }
}