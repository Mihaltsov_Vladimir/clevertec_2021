package com.example.myapplication.presentation


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.domain.PointModel
import com.example.myapplication.data.repository.PointRepositoryImpl
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject


class MapsViewModel @Inject constructor(
    private val pointRepositoryImpl: PointRepositoryImpl
) : ViewModel() {

    private lateinit var nullPointViewModel: LatLng
    private val status: MutableLiveData<String> = MutableLiveData()
    private val mutableLiveData: MutableLiveData<List<PointModel>> = MutableLiveData<List<PointModel>>()



    fun liveDataPoints(nullPoint: LatLng): LiveData<List<PointModel>> = mutableLiveData.also {
        nullPointViewModel = nullPoint
    }


    fun liveDataStatus(): LiveData<String> = status


     fun loadLatLng(city: String): Disposable = Single.zip(
        pointRepositoryImpl.getFilials(city),
        pointRepositoryImpl.getAtm(city),
        pointRepositoryImpl.getInfo(city)
    ) { atm, filial, infoBox ->
        val list: MutableList<PointModel> = mutableListOf()
        list.addAll(atm)
        list.addAll(filial)
        list.addAll(infoBox)
        list.sortWith { p0, p1 ->
            val firstPoint =
                SphericalUtil.computeDistanceBetween(nullPointViewModel, LatLng(p0.gpsX, p0.gpsY))
            val nextPoint =
                SphericalUtil.computeDistanceBetween(nullPointViewModel, LatLng(p1.gpsX, p1.gpsY))
            (firstPoint - nextPoint).toInt()
        }
        return@zip list
    }
        .map { it.take(10) }
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
            mutableLiveData.postValue(it)
        },
            {
                status.postValue(it.toString())
            })
}



