package com.example.myapplication.presentation.di.compnent

import android.app.Application
import com.example.myapplication.AppController
import com.example.myapplication.presentation.di.module.ActivityModule
import com.example.myapplication.presentation.di.module.ApiModule
import com.example.myapplication.presentation.di.module.RepositoryModule
import com.example.myapplication.presentation.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        ApiModule::class,
        ViewModelModule::class,
        RepositoryModule::class,
        ActivityModule::class,
        AndroidSupportInjectionModule::class]
)
@Singleton
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(appController: AppController)
}