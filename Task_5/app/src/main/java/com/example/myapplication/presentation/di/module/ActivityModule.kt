package com.example.myapplication.presentation.di.module

import com.example.myapplication.presentation.MapsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector()
    abstract fun contributeMainActivity(): MapsActivity
}