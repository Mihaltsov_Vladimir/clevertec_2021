package com.example.myapplication.presentation.di.module

import com.example.myapplication.data.repository.IRepository
import com.example.myapplication.data.repository.PointRepositoryImpl
import com.example.myapplication.data.services.BankApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideBankPointsRepository(apiService: BankApiService): IRepository =
        PointRepositoryImpl(apiService)

}