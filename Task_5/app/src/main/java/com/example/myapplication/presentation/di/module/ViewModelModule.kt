package com.example.myapplication.presentation.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.presentation.di.ViewModelKey
import com.example.myapplication.presentation.MapsViewModel
import com.example.myapplication.presentation.factory.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MapsViewModel::class)
    protected abstract fun movieListViewModel(moviesListViewModel: MapsViewModel): ViewModel
}