package com.example.task7.presentation.adapter

import android.R
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.example.task7.domain.model.FieldData
import com.example.task7.domain.model.ValuesData
import com.example.task7.databinding.ForListTypeBinding
import com.example.task7.databinding.ForNumericTypeBinding
import com.example.task7.databinding.ForTextTypeBinding


class AdapterRecyclerViewMain(private val metaDataModel: MutableList<FieldData.Base>) :
    RecyclerView.Adapter<AdapterRecyclerViewMain.BaseViewHolder<FieldData.Base>>() {

    private var _request = HashMap<String, String>(3)
    fun requestFromAdapter() = _request

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<FieldData.Base> {

        return when (viewType) {
            0 -> {
                val binding = ForTextTypeBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
                HolderString(binding)
            }
            1 -> {
                val binding = ForNumericTypeBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
                HolderNumeric(binding)
            }
            2 -> {
                val binding = ForListTypeBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
                HolderList(binding)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount() = metaDataModel.size

    abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(item: T)

        fun EditText.listenChanges(block: (text: String) -> Unit) {
            addTextChangedListener(object : SimpleTextWatcher() {
                override fun afterTextChanged(p0: Editable?) {
                    block.invoke(p0.toString())
                }
            })
        }
    }

    inner class HolderString(private val binding: ForTextTypeBinding) :
        BaseViewHolder<FieldData.Base>(binding.root) {

        override fun bind(item: FieldData.Base) {
            binding.textString.text = item.map(FieldData.Mapper.TitleToAdapterRW())
            binding.editString.listenChanges {
                _request[item.map(FieldData.Mapper.NameToAdapterRW())] = it
            }
        }
    }

    inner class HolderNumeric(private val binding: ForNumericTypeBinding) :
        BaseViewHolder<FieldData.Base>(binding.root) {

        override fun bind(item: FieldData.Base) {
            binding.textNumeric.text = item.map(FieldData.Mapper.TitleToAdapterRW())
            binding.editString.listenChanges {
                _request[item.map(FieldData.Mapper.NameToAdapterRW())] = it
            }
        }
    }

    inner class HolderList(private val binding: ForListTypeBinding) :
        BaseViewHolder<FieldData.Base>(binding.root) {

        override fun bind(item: FieldData.Base) {
            binding.textList.text = item.map(FieldData.Mapper.TitleToAdapterRW())

            val adapter =
                ArrayAdapter(
                    binding.root.context,
                    R.layout.simple_spinner_item,
                    item.map(FieldData.Mapper.ListToAdapterRW())
                )
            binding.spinnerListType.adapter = adapter
            binding.spinnerListType.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    if (item.map(FieldData.Mapper.ListValuesToAdapterRW()).isNotEmpty())
                        _request[item.map(FieldData.Mapper.NameToAdapterRW())] =
                            item
                                .map(FieldData.Mapper.ListValuesToAdapterRW())[p2]
                                .map(ValuesData.Mapper.KeyToAdapterRW())
                }
                override fun onNothingSelected(p0: AdapterView<*>?) {
                }
            }
        }
    }

    override fun getItemViewType(position: Int) =
        metaDataModel[position].map(FieldData.Mapper.TypeToAdapterRW())

    override fun onBindViewHolder(holder: BaseViewHolder<FieldData.Base>, position: Int) {
        val element = metaDataModel[position]
        when (holder) {
            is HolderString -> holder.bind(element)
            is HolderNumeric -> holder.bind(element)
            is HolderList -> holder.bind(element)
            else -> throw IllegalArgumentException()
        }
    }
}





