package com.example.task7.presentation.di.module

import com.example.task7.domain.repository.IRepository
import com.example.task7.data.repository.MainRepository
import com.example.task7.data.services.DataApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideRepository(apiService: DataApiService): IRepository = MainRepository(apiService)
}