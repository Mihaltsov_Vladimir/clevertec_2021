package com.example.task7.presentation.di.module

import com.example.task7.domain.repository.IRepository
import com.example.task7.domain.usecase.GetDataUseCase
import com.example.task7.domain.usecase.IGetDataUseCase
import com.example.task7.domain.usecase.ISendDataUseCase
import com.example.task7.domain.usecase.SendDataUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {

    @Singleton
    @Provides
    fun provideGetDataUseCase(repository: IRepository): IGetDataUseCase = GetDataUseCase(repository)

    @Singleton
    @Provides
    fun provideSendDataUseCase(repository: IRepository): ISendDataUseCase = SendDataUseCase(repository)
}