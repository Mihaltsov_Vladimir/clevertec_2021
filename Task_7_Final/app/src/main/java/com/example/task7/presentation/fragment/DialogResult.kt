package com.example.task7.presentation.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment
import com.example.task7.R

private const val ARG_PARAM1 = "param1"

class DialogResult() : AppCompatDialogFragment() {

    private var responseServer: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            responseServer = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(requireContext())
            .setMessage(responseServer)
            .setNegativeButton(R.string.cancel) { dialog, id -> dismiss() }
            .setCancelable(false)
            .setTitle(R.string.response)
            .create()

    companion object {
        @JvmStatic
        fun newInstance(param1: String) =
            DialogResult().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                }
            }
    }
}

