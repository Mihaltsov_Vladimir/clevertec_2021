package com.example.task7.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.task7.R
import com.example.task7.domain.model.ModelData
import com.example.task7.databinding.FragmentMainBinding
import com.example.task7.domain.model.ModelResponseData
import com.example.task7.presentation.adapter.AdapterRecyclerViewMain
import com.example.task7.presentation.view_model.MainViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class MainFragment : Fragment() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var mainViewModel: MainViewModel
    private var adapterRecyclerViewMain: AdapterRecyclerViewMain? = null
    private var keySave = ""
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private var flagShowDialog: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        keySave = resources.getString(R.string.key_save)
        AndroidSupportInjection.inject(this)
        mainViewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]
        savedInstanceState?.let { it -> flagShowDialog = it.getBoolean(keySave, true) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.sendButton.setOnClickListener {
            adapterRecyclerViewMain?.let { it1 ->
                mainViewModel.sendData(it1.requestFromAdapter())
                flagShowDialog = true
            }
        }
        initialiseViewModel()
    }

    private fun initialiseViewModel() {

        mainViewModel.getDataOfCloud().observe(viewLifecycleOwner) { modelData ->
            showImage(modelData.map(ModelData.Mapper.ImageToMainFragment()))
            adapterRecyclerViewMain =
                AdapterRecyclerViewMain(modelData.map(ModelData.Mapper.FieldToMaiFragment()))
            binding.recyclerView.adapter = adapterRecyclerViewMain
            binding.mainToolbar.title = modelData.map(ModelData.Mapper.TitleToMainFragment())
        }

        mainViewModel.getStateProgressBar().observe(viewLifecycleOwner) {
            if (it) {
                binding.scrollView.visibility = View.GONE
                binding.metaDataImage.visibility = View.GONE
                binding.mainProgressBar.visibility = View.VISIBLE
            } else {
                binding.scrollView.visibility = View.VISIBLE
                binding.metaDataImage.visibility = View.VISIBLE
                binding.mainProgressBar.visibility = View.GONE
            }
        }

        mainViewModel.getResultPushData().observe(viewLifecycleOwner) {
            if (flagShowDialog) {
                DialogResult.newInstance(it.map(ModelResponseData.Mapper.AllResponse()))
                    .show(childFragmentManager, null)
                flagShowDialog = false
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(keySave, flagShowDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun showImage(url: String) {
        Glide.with(binding.metaDataImage).load(url).into(binding.metaDataImage)
    }
}