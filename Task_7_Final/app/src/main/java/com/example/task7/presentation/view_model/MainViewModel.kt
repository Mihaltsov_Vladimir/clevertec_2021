package com.example.task7.presentation.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.task7.domain.model.ModelData
import com.example.task7.domain.model.ModelResponseData
import com.example.task7.domain.usecase.IGetDataUseCase
import com.example.task7.domain.usecase.ISendDataUseCase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val sendDataUseCase: ISendDataUseCase,
    private val getDataUseCase: IGetDataUseCase
) : ViewModel() {

    private val _dataOfCloud: MutableLiveData<ModelData> by lazy {
        MutableLiveData<ModelData>().also {
            makeApiCall()
        }
    }
    private val _loadingDataProgressBar: MutableLiveData<Boolean> = MutableLiveData()
    private val _resultPushData: MutableLiveData<ModelResponseData> = MutableLiveData()

    fun getDataOfCloud(): LiveData<ModelData> = _dataOfCloud
    fun getStateProgressBar(): LiveData<Boolean> = _loadingDataProgressBar
    fun getResultPushData(): LiveData<ModelResponseData> = _resultPushData

    private fun makeApiCall() {
        getDataUseCase.execute()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _loadingDataProgressBar.value = true
            }
            .subscribe({
                _dataOfCloud.value = it
                _loadingDataProgressBar.value = false
            }, {
            })
    }

    fun sendData(userChoice: HashMap<String, String>) {
        sendDataUseCase.execute(userChoice)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                _loadingDataProgressBar.value = true
            }
            .subscribe({
                _resultPushData.value = it
                _loadingDataProgressBar.value = false
            }, {
//                _resultPushData.value = ModelResponseData.Base().map(ModelResponseData.Mapper.ThrowableResponse(it.toString()))//todo
                _loadingDataProgressBar.value = false

            })
    }
}