package com.example.task7.data.model

import com.example.task7.domain.model.TypeFieldData
import com.google.gson.annotations.SerializedName

interface FieldDTO {

    fun <T> map(mapper: Mapper<T>): T

    class Base(
        @SerializedName("title")
        private val title: String,

        @SerializedName("name")
        private val name: String,

        @SerializedName("type")
        private val type: String,

        @SerializedName("values")
        private val metaValues: HashMap<String, String>? = null
    ) : FieldDTO {
        override fun <T> map(mapper: Mapper<T>) = mapper.map(title, name, type, metaValues)
    }


    interface Mapper<T> {
        fun map(title: String, name: String, type: String, metaValues: HashMap<String, String>?): T

        class TitleToModelDTO : Mapper<String> {
            override fun map(
                title: String,
                name: String,
                type: String,
                metaValues: HashMap<String, String>?
            ) = title
        }

        class NameToModelDTO : Mapper<String> {
            override fun map(
                title: String,
                name: String,
                type: String,
                metaValues: HashMap<String, String>?
            ) = name
        }

        class TypeToModelDTO : Mapper<TypeFieldData> {
            override fun map(
                title: String,
                name: String,
                type: String,
                metaValues: HashMap<String, String>?
            ): TypeFieldData {
                var type1 = TypeFieldData.LIST
                if (type == TypeFieldData.TEXT.name) {
                    type1 = TypeFieldData.TEXT
                }
                if (type == TypeFieldData.NUMERIC.name) {
                    type1 = TypeFieldData.NUMERIC
                }
                return type1
            }
        }

        class MetaValuesToModelDTO : Mapper<HashMap<String, String>> {
            override fun map(
                title: String,
                name: String,
                type: String,
                metaValues: HashMap<String, String>?
            ): HashMap<String, String> {
                return metaValues ?: HashMap()
            }
        }
    }
}



