package com.example.task7.data.model

import com.example.task7.domain.model.FieldData
import com.example.task7.domain.model.ModelData
import com.example.task7.domain.model.ValuesData
import com.google.gson.annotations.SerializedName


interface ModelDTO {

    fun <T> map(mapper: Mapper<T>): T

    class Base(
        @SerializedName("title")
        private val title: String,

        @SerializedName("image")
        private val image: String,

        @SerializedName("fields")
        private val metaFields: List<FieldDTO.Base>
    ) : ModelDTO {
        override fun <T> map(mapper: Mapper<T>) =
            mapper.map(title, image, metaFields)
    }


    interface Mapper<T> {
        fun map(title: String, image: String, metaFields: List<FieldDTO.Base>): T

        class ModelToDomain : Mapper<ModelData> {
            override fun map(
                title: String,
                image: String,
                metaFields: List<FieldDTO.Base>
            ): ModelData {
                val listFieldData: MutableList<FieldData.Base> = mutableListOf()
                val listValuesData: MutableList<ValuesData.Base> = mutableListOf()
                val listValuesForAdapter: ArrayList<String> = arrayListOf()
                metaFields.forEach {
                    if (it.map(FieldDTO.Mapper.MetaValuesToModelDTO()).isNotEmpty()) {
                        for ((key, value) in it.map(FieldDTO.Mapper.MetaValuesToModelDTO())) {
                            listValuesData.add(ValuesData.Base(key, value))
                            listValuesForAdapter.add(value)
                        }
                    }
                    listFieldData.add(
                        FieldData.Base(
                            title = it.map(FieldDTO.Mapper.TitleToModelDTO()),
                            name = it.map(FieldDTO.Mapper.NameToModelDTO()),
                            type = it.map(FieldDTO.Mapper.TypeToModelDTO()),
                            listAdapter = listValuesForAdapter,
                            listValues = listValuesData
                        )
                    )
                }
                return ModelData.Base(
                    title = title,
                    image = image,
                    dataField = listFieldData
                )
            }
        }
    }
}

