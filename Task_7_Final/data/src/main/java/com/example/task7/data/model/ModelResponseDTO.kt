package com.example.task7.data.model

import com.example.task7.domain.model.ModelResponseData
import com.google.gson.annotations.SerializedName

interface ModelResponseDTO {

    fun <T> map(mapper: Mapper<T>): T

    class Base(
        @SerializedName("result")
        val result: String
    ) : ModelResponseDTO {
        override fun <T> map(mapper: Mapper<T>) = mapper.map(result)
    }


    interface Mapper<T> {
        fun map(result: String): T

        class ToModelResponseData : Mapper<ModelResponseData> {
            override fun map(
                result: String
            ): ModelResponseData.Base {
                return ModelResponseData.Base(response = result)
            }
        }
    }
}
