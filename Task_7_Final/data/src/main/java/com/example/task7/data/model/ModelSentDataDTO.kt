package com.example.task7.data.model

import com.google.gson.annotations.SerializedName


data class ModelSentDataDTO(

    @SerializedName("form")
    val form: HashMap<String, String>
)



