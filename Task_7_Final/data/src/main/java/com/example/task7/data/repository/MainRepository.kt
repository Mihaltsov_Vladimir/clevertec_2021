package com.example.task7.data.repository

import com.example.task7.data.model.ModelDTO
import com.example.task7.domain.model.ModelData
import com.example.task7.data.model.ModelResponseDTO
import com.example.task7.data.model.ModelSentDataDTO
import com.example.task7.data.services.DataApiService
import com.example.task7.domain.model.ModelResponseData
import com.example.task7.domain.repository.IRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(
    private val dataApiService: DataApiService
) : IRepository {

    override fun getMetaDataCloud(): Single<ModelData> {
        return dataApiService.getRequestMeta()
            .map { it.map(ModelDTO.Mapper.ModelToDomain()) }
    }

    override fun sendDataCloud(userChoice: HashMap<String, String>): Single<ModelResponseData> {
        return dataApiService.sentData(ModelSentDataDTO(userChoice))
            .map { it.map(ModelResponseDTO.Mapper.ToModelResponseData()) }
    }
}
