package com.example.task7.data.services


import com.example.task7.data.model.ModelDTO
import com.example.task7.data.model.ModelResponseDTO
import com.example.task7.data.model.ModelSentDataDTO
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface DataApiService {

    @GET("meta")
    fun getRequestMeta(): Single<ModelDTO.Base>

    @POST("data/")
    fun sentData(@Body request: ModelSentDataDTO): Single<ModelResponseDTO.Base>
}