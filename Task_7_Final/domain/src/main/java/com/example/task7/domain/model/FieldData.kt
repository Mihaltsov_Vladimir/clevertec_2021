package com.example.task7.domain.model

interface FieldData {

    fun <T> map(mapper: Mapper<T>): T

    class Base(
        private val title: String,
        private val name: String,
        private val type: TypeFieldData,
        private val listAdapter: List<String>,
        private val listValues: List<ValuesData.Base>? = null
    ) : FieldData {
        override fun <T> map(mapper: Mapper<T>) =
            mapper.map(title, name, type, listAdapter, listValues)
    }


    interface Mapper<T> {
        fun map(
            title: String,
            name: String,
            type: TypeFieldData,
            listAdapter: List<String>,
            listValues: List<ValuesData.Base>?
        ): T

        class TitleToAdapterRW : Mapper<String> {
            override fun map(
                title: String,
                name: String,
                type: TypeFieldData,
                listAdapter: List<String>,
                listValues: List<ValuesData.Base>?
            ) = title
        }

        class NameToAdapterRW : Mapper<String> {
            override fun map(
                title: String,
                name: String,
                type: TypeFieldData,
                listAdapter: List<String>,
                listValues: List<ValuesData.Base>?
            ) = name
        }

        class TypeToAdapterRW : Mapper<Int> {
            override fun map(
                title: String,
                name: String,
                type: TypeFieldData,
                listAdapter: List<String>,
                listValues: List<ValuesData.Base>?
            ) = type.int
        }

        class ListToAdapterRW : Mapper<List<String>> {
            override fun map(
                title: String,
                name: String,
                type: TypeFieldData,
                listAdapter: List<String>,
                listValues: List<ValuesData.Base>?
            ) = listAdapter
        }

        class ListValuesToAdapterRW : Mapper<List<ValuesData.Base>> {
            override fun map(
                title: String,
                name: String,
                type: TypeFieldData,
                listAdapter: List<String>,
                listValues: List<ValuesData.Base>?
            ): List<ValuesData.Base> {
                return listValues ?: listOf()
            }


        }
    }
}
