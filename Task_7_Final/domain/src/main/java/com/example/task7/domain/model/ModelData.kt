package com.example.task7.domain.model


interface ModelData {

    fun <T> map(mapper: Mapper<T>): T

    class Base(
        private val title: String,
        private val image: String,
        private val dataField: MutableList<FieldData.Base>
    ) : ModelData {
        override fun <T> map(mapper: Mapper<T>) = mapper.map(title, image, dataField)

    }

    interface Mapper<T> {
        fun map(
            title: String,
            image: String,
            dataField: MutableList<FieldData.Base>
        ): T


        class ImageToMainFragment : Mapper<String> {
            override fun map(
                title: String,
                image: String,
                dataField: MutableList<FieldData.Base>
            ) = image
        }

        class TitleToMainFragment : Mapper<String> {
            override fun map(
                title: String,
                image: String,
                dataField: MutableList<FieldData.Base>
            ) = title
        }

        class FieldToMaiFragment : Mapper<MutableList<FieldData.Base>> {
            override fun map(
                title: String,
                image: String,
                dataField: MutableList<FieldData.Base>
            ) = dataField

        }
    }

}


