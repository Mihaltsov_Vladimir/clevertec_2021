package com.example.task7.domain.model

interface ModelResponseData {

    fun <T> map(mapper: Mapper<T>): T

    class Base(private val response: String
    ) : ModelResponseData {
        override fun <T> map(mapper: Mapper<T>) = mapper.map(response)
    }

    interface Mapper<T> {
        fun map(response: String): T

        class AllResponse : Mapper<String> {
            override fun map(response: String) = response
        }

        class ThrowableResponse(private val throwable: String) : Mapper<String> {
            override fun map(response: String) = throwable
        }
    }
}