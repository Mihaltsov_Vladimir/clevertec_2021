package com.example.task7.domain.model

enum class TypeFieldData(val int: Int) {
    TEXT(0),
    NUMERIC(1),
    LIST(2)
}
