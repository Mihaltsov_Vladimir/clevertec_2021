package com.example.task7.domain.model

interface ValuesData {

    fun <T> map(mapper: Mapper<T>): T


    class Base(
        private val key: String,
        private val value: String
    ) : ValuesData {
        override fun <T> map(mapper: Mapper<T>) = mapper.map(key, value)

    }

    interface Mapper<T> {

        fun map(key: String, value: String): T

        class KeyToAdapterRW : Mapper<String> {
            override fun map(key: String, value: String) = key
        }
    }

}