package com.example.task7.domain.repository

import com.example.task7.domain.model.ModelData
import com.example.task7.domain.model.ModelResponseData
import io.reactivex.rxjava3.core.Single

interface IRepository {

    fun getMetaDataCloud(): Single<ModelData>

    fun sendDataCloud(userChoice: HashMap<String, String>): Single<ModelResponseData>
}
