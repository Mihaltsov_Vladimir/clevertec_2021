package com.example.task7.domain.usecase

import com.example.task7.domain.model.ModelData
import com.example.task7.domain.repository.IRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetDataUseCase @Inject constructor(
    private val repository: IRepository
) : IGetDataUseCase {
    override fun execute(): Single<ModelData> = repository.getMetaDataCloud()
}
