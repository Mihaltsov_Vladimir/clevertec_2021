package com.example.task7.domain.usecase

import com.example.task7.domain.model.ModelData
import io.reactivex.rxjava3.core.Single

interface IGetDataUseCase {
    fun execute(): Single<ModelData>
}