package com.example.task7.domain.usecase

import com.example.task7.domain.model.ModelResponseData
import io.reactivex.rxjava3.core.Single

interface ISendDataUseCase {
    fun execute(userChoice: HashMap<String, String>): Single<ModelResponseData>
}