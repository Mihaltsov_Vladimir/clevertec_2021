package com.example.task7.domain.usecase

import com.example.task7.domain.model.ModelResponseData
import com.example.task7.domain.repository.IRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SendDataUseCase @Inject constructor(
    private val repository: IRepository
) : ISendDataUseCase {
    override fun execute(userChoice: HashMap<String, String>): Single<ModelResponseData> =
        repository.sendDataCloud(userChoice)
}

